#!/usr/bin/perl -w
use strict;

my $min_mapq = 20;

my $infile = "HG00733_GRCh38_GoNL_uns.bam";
my $outfile = $infile;
$outfile =~ s/\.bam$//;
$outfile.= '_nonref.bam';

my ( $i, $j )  = ( 0, 0 );
open F, 'samtools view -h -q '.$min_mapq.' '.$infile.' |';
open F1, '| samtools view -bS - >'.$outfile;
while ( <F> ) {
    if ( m/^\@/ ) {
        print F1;
        next;
    }
    my ( $read, $flag, $chr, $pos ) = split /\t/;
    warn "Parsing through $chr\:$pos\n" unless ++$i % 1_000_000;
    #next if $chr =~ m/^chr[\dXYMUn]{1,2}/;
    next unless $chr =~ m/^scaf/ or $chr =~ m/^C\d+/;
    $j++;
    print F1;
}
close F;
close F1;
warn "Done! Saved $j reads from $i\n"; 


