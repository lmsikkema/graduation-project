#!/usr/bin/perl -w
use strict;

foreach my $bam_file ( <*.bam> ) {
    my $lib = substr( $bam_file, 0, 7);
    next if -s $lib.'1.fq' or -s $lib.'1.fq.gz';
    warn "Busy with $lib\n";
    system( 'samtools sort -n -m 5G '.$bam_file.' | samtools fastq -0 '.$lib.'_0.fq -1 '.$lib.'_1.fq -2 '.$lib.'_2.fq -' );
    warn "gzipping 1\n";
    system( 'gzip', $lib.'_1.fq' );
    warn "gzipping 2\n";
    system( 'gzip', $lib.'_2.fq' );
}
    
    
