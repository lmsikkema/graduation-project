#!/usr/bin/perl -w
use strict;

my $min_mapq = 20;

my %barcode2ctg = ();

my $infile = "HG00733_GRCh38_GoNL_uns_nonref_ctg_barcode.txt";
my $BAMinfile = "HG00733_WGS_phased_possorted_bam.bam";
my $outfile = $infile;
$outfile =~ s/_barcode\.txt$/_genomelocus\.txt/;



warn "Reading barcode ctg and barcode info\n";
open F, '<', $infile;
while ( <F> ) {
    chomp;
    my ( $ctg, @barcodes ) = split /\t/;
    foreach my $bc_string ( @barcodes ) {
        my ( $barcode, $count ) = split /\:/, $bc_string;
        $barcode2ctg{$barcode}{$ctg} += $count;
    }
}
close F;
warn scalar keys %barcode2ctg, " barcodes needed\n";

my $i = 0;
my %ctg2genomelocus = ();
warn "Reading 10x BAM file for genomic locations\n";
open F, 'samtools view -q '.$min_mapq.' '.$BAMinfile.'|';
while ( <F> ) {
    my ( $read, $map_flag, $chr, $pos ) = split /\t/;
    my ( $barcode ) = m/BX\:Z\:([GATC]+)/;
    next unless defined( $barcode );
    next unless exists( $barcode2ctg{$barcode} );
    warn "$i done\n" unless ++$i % 10_000;
    foreach my $ctg ( keys %{ $barcode2ctg{$barcode} } ) {
        $ctg2genomelocus{$ctg}{$chr.':'. int($pos/1000) } += $barcode2ctg{$barcode}{$ctg};
    }
}
close F;

warn "Outputting results\n";
open F, '>', $outfile;
foreach my $ctg ( sort keys %ctg2genomelocus ) {
    print F $ctg;
    foreach my $locus ( sort {$ctg2genomelocus{$ctg}{$b}<=>$ctg2genomelocus{$ctg}{$a}} keys %{$ctg2genomelocus{$ctg}} ) {
        print F "\t", $locus.':'.$ctg2genomelocus{$ctg}{$locus};
    }
    print F "\n";
}
close F;



