#!/usr/bin/perl -w
use strict;

my $min_mapq = 20;

my %ctg2read = ();

warn "Reading BAM\n";

my $infile = "HG00733_GRCh38_GoNL_uns_nonref_sorted.bam";
my $outfile = $infile;
$outfile =~ s/_sorted\.bam$/_ctg_reads\.txt/;


open F, 'samtools view -q '.$min_mapq.' '.$infile.' |';
while ( <F> ) {
    my ( $read, $map_flag, $ctg, $ctg_pos ) = split /\t/;
    $ctg2read{$ctg}{$read}=1;
}
close F;

warn "Writing results\n";
open F, '>', $outfile;
foreach my $ctg ( sort keys %ctg2read ) {
    print F join( "\t", $ctg, sort keys %{$ctg2read{$ctg}} ), "\n";
}
close F;

