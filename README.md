# Mapping 'dark matter DNA' to the human genome reference
### *Graduation project of Lisanne Sikkema*
### **Field of study:** Medical Diagnostics
#### **Institute:** Hanze University of Applied Sciences, Groningen
### **Internship:** European Research Institute for the Biology of Ageing, Groningen
#### **Group:** Genome Structure Ageing
### **Supervisors:** Victor Guryev and Titia Vrenken
### **Period:** April 2019 till October 2019

  
  
## ** Scripts **
### ** Alignment of 10X Chromium linked read data to the reference **
###  01_extract_fastq.pl 
##### *** This Perl script converts the 10X Chromium linked-reads BAM file to a gzipped fastq file ***
- ** Software: **  
            * Perl  
            * Samtools  
            * gzip
- ** Run from commandline: **
> perl 01_extract_fastq.pl <BAM_file>
- ** Input: ** 10X Chromium linked-reads BAM file
- ** Output: ** Gzipped fastq file with sample name + suffix 1.fq.gz, 2.fq.gz
        
     
### ** Extracting reads **
### 02_extract_nonref_reads.pl
##### *** This Perl script extracts the reads that are not aligned to the reference***
- ** Software: **  
            * Perl  
            * Samtools  
- ** Run from commandline: **
> perl 02_extract_nonref_reads.pl 
- ** Input: ** Alignment file
- ** Output: ** Alignment BAM file with alignment sample name + suffix *nonref.bam*  


### 03_extract_ctg_reads.pl
##### *** This Perl script extracts the contigs of the non-reference reads ***
- ** Software: **  
            * Perl  
            * Samtools  
- ** Run from commandline: **
> perl 03_extract_ctg_reads.pl
- ** Input: ** Alignment BAM file with sorted non-reference reads (*nonref_sorted.bam*)
- ** Output: ** 
	* File with contigs of non-reference reads 
	* Name: Sample name + suffix *ctg_reads.txt*
  
## Find genomic location
### 04_find_barcodes.pl
##### *** This Perl script finds the 10X Chromium barcodes of the non-reference reads ***
- ** Software: **  
            * Perl  
            * Samtools  
- ** Run from commandline: **
> perl 04_find_barcodes.pl
- ** Input: ** 
	* File with contigs of non-reference reads (*nonref_ctg.txt*)
	* 10X Chromium BAM file
- ** Output: ** 
	* File with contigs and barcodes 
	* Name: non-reference sample name + suffix *ctg_barcode.txt*
        
### 05_find_genomic_locus.pl
##### *** This Perl script reads the 10X Chromium barcodes of the non-reference reads to find genomic position ***
- ** Software: **  
            * Perl  
            * Samtools  
- ** Run from commandline: **
> perl 05_find_genomic_locus.pl
- ** Input: ** 
	* File with contigs and barcodes of non-reference reads (*ctg_barcode.txt*)
	* 10X Chromium BAM file
- ** Output: ** 
	* File with contigs and genomic location
	* Name: non-reference sample name + suffix *ctg_genomelocus.txt*
        
### 06_check_genomic_locus.py
##### *** This Python script checks the genomic position and compares if the first two reads are mapped to the same chromosomal position ***
- ** Software: ** Python 3
- ** Run from commandline: **
> python3 06_check_genomic_locus.py
- ** Input: ** File with contigs and barcodes of non-reference reads (*ctg_barcode.txt*)
- ** Output: ** 
	* 3 files: 
		1. Same chomosomal position  
		2. Different chromosomal position  
		3. Error file to check chromosomal position manually
	* Name: Sample name + suffix *same/diff_chr.txt* or *err.txt*
        
### 07_check_position.py
##### *** This Python script checks if the position and compares if the first two reads are mapped to the same chromosomal position  ***
- ** Software: ** Python 3
- ** Run from commandline: **
> python3 07_check_position.py
- ** Input: ** File with same chromosomal position (*same_chr.txt*)
- ** Output: ** 
	* Text file with contigs and reads with genomic positons that differ less than 100 kbp. 
	* Name: Sample name + suffix *same_genloc.txt* 

## Gene annotation
### 08_merging_files.py
##### *** This Python script merges all files, gets the position with the biggest number of reads and checks if the read next to that position accounts for >90% of the reads ***
- ** Software: ** Python 3
- ** Run from commandline: **
> python3 08_merging_files.py
- ** Input: ** Files with reads with genomic positions that differ less than 100 kbp from all samples (*same_genloc.txt*)
- ** Output: ** Text file with columns:  
		1. Contig name  
		2. Chromosomal location (best)  
		3. Position (best)  
		4. Reads on target  
		5. Reads in total  
		6. Number of hits  
		7. Percentage (x100%)  
        
### 09_find_genes.py
##### *** This Python script finds the overlap of contigs with genes from BioMart ***
- ** Software: ** Python 3
- ** Run from commandline: **
> python3 09_find_genes.py
- ** Input: ** 
	* Text file with merged calls
	* BioMart file with human genes
- ** Output: ** Text file with columns:  
		1. Contig name  
		2. Chromosomal location (best)  
		3. Position (best)  
		4. Gene name  
		5. Gene start position  
		6. Gene end position  
		7. Gene type  
        
### 10_counting lines.py
##### *** This Python script counts how much contigs have overlap with genes ***
- ** Software: ** Python 3
- ** Run from commandline: **
> python3 10_counting lines.py
- ** Input: ** 
	* Text file with merged calls
	* Text file with the overlap between contigs and BioMart genes
- ** Output: ** Number of contigs that are in the vicinity of a gene


## Programming languages used: 
#### Script 1 to 5: Perl v5.26.1
#### Script 6 to 10: Python v3.7.3


Note: Filenames of the script need to be changed before using the script