#!/usr/bin/env python3

mart = {}
with open("mart_export_protcod.txt", "r") as fmart:
    for gene in fmart:
        [chrom, start, end, name, genetype] = gene.split("\t")
        chrom = 'chr{}'.format(chrom)
        if gene not in mart:
            mart[gene] = []
                
        mart[gene].append(chrom) 
        mart[gene].append(start) 
        mart[gene].append(end) 
        mart[gene].append(name) 
        mart[gene].append(genetype)

        



    segm = {}
    with open("merged_calls.txt", "r") as merge:
        for contig in merge:
            [cname, chrm, pos, on_target, total,hits, percentage] = contig.split("\t")
            if contig not in segm:
                segm[contig] = []

            segm[contig].append(cname) 
            segm[contig].append(chrm) 
            segm[contig].append(pos)            
                

        with open("genes_protcod.txt", "w") as result:
            result.write("Contig" + "\t" + "Chromosome" + "\t" + "Position" + "\t" + "Gene name" +"\t" + "Gene start (bp)" + "\t" + "Gene end (bp)" +  "\t" + "Gene type" + "\n")
            for gene in mart:
                for contig in segm:
                    if mart[gene][0] == segm[contig][1] and int(mart[gene][1]) < int(segm[contig][2]) < int(mart[gene][2]):
                        result.write(segm[contig][0] + "\t" + mart[gene][0] + "\t" + segm[contig][2] + "\t" + mart[gene][3] + "\t" + mart[gene][1] + "\t" + mart[gene][2] + "\t" +  mart[gene][4] )
                        print("contig", segm[contig][0], "is in gene", mart[gene][3] )



                    

