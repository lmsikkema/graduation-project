#!/usr/bin/perl -w
use strict;

my %read2ctg = ();

my $infile = "HG00733_GRCh38_GoNL_uns_nonref_ctg_reads.txt";
my $BAMinfile = "HG00733_WGS_phased_possorted_bam.bam";
my $outfile = $infile;
$outfile =~ s/_reads\.txt$/_barcode\.txt/;



warn "Reading read to contig data\n";
open F, '<', $infile;
while ( <F> ) {
    chomp;
    my ( $ctg, @reads ) = split /\t/;
    foreach my $read ( @reads ) {
        $read2ctg{$read} = $ctg;
    }
}
close F;
warn scalar keys %read2ctg, " reads will be checked in 10x BAM file\n";

warn "Parsing 10x BAM file\n";
my $i = 0;
my %ctg2barcode = ();
open F, 'samtools view '.$BAMinfile.'|';
while ( <F> ) {
    my ( $read ) = split /\t/;
    next unless exists( $read2ctg{$read} );
    my ( $barcode ) = m/BX\:Z\:([GATC]+)/;
    next unless defined($barcode);
    my $ctg = $read2ctg{$read};
    $ctg2barcode{$ctg}{$barcode}++;
    warn "$i reads done\n" unless ++$i % 10_000;
}
close F;

warn "Outputting results\n";
open F, '>', $outfile;
foreach my $ctg ( sort keys %ctg2barcode ) {
    print F $ctg;
    foreach my $barcode ( sort keys %{$ctg2barcode{$ctg}} ) {
        print F "\t", $barcode,':', $ctg2barcode{$ctg}{$barcode};
    }
    print F "\n";
}
close F;


