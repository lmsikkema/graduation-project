#!/usr/bin/env python3

with open("merged_calls.txt") as ft:
    line = ft.readline()
    cnt = 1
    while line:
        line = ft.readline()
        cnt += 1
    print(cnt, "contigs total")

with open("results_genes.txt") as fg:
    line = fg.readline()
    cnt = 1
    while line:
        line = fg.readline()
        cnt += 1
    print(cnt, "contigs in genes")

with open("results_genes_protcod.txt") as fp:
    line = fp.readline()
    cnt = 1
    while line:
        line = fp.readline()
        cnt += 1
    print(cnt, "contigs in protein-coding genes")



