#!/usr/bin/env python3

infile = 'NA19240_same_chr.txt'
outfile = 'NA19240_same_genloc.txt'

with open(infile, 'r') as file: 
    for line in file: 
        str = line.split()
        first = str[1]
        split1 = first.split(":")
        pos1 = split1[1]
        
        second = str[2]
        split2 = second.split(":")
        pos2 = split2[1]
        
        compare = int(pos2)-int(pos1)
        if compare > -100 and compare < 100:
            with open(outfile, 'a+') as f: 
                f.write(line)



