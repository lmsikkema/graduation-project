#!/usr/bin/env python3

infile = "HG00512_nonref_ctg_genomelocus.txt"

with open(infile, "r") as file:
    for line in file:
        try: 
            str = line.split()
            #splitting data to get chromosome and position
            data1 = str[1]
            first = data1.split(":")
            chr1 = first[0]
            pos1 = first[1]

            data2 = str[2]
            second = data2.split(":")
            chr2 = second[0]
            pos2 = second[1]
            
        #check if first two of every contig are the same chromosome and position
            if chr1 == chr2:
                with open("HG00512_same_chr.txt", "a+") as file: 
                    file.write(str[0])
                    file.write(" ")
                    file.write(data1)
                    file.write(" ")
                    file.write(data2) 
                    file.write("\n")
                    
            else: 
                with open("HG00512_diff_chr.txt", "a+") as file:
                    file.write(str[0])
                    file.write(" ")
                    file.write(data1)
                    file.write(" ")
                    file.write(data2)
                    file.write("\n")

        except: 
            with open("HG00512_err.txt", "a+") as errfile:
                errfile.write(str[0])
                errfile.write(" ")
                errfile.write(data1)
                errfile.write(" ")
                errfile.write(data2)
                errfile.write("\n")


        
