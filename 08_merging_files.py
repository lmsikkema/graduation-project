#!/usr/bin/env python3

# Settings
max_distance = 40
min_on_target_read_fraction = 0.9
min_on_target = 100

# Files
filenames = ["HG00512_same_genloc.txt",
             "HG00513_same_genloc.txt",
             "HG00514_same_genloc.txt",
             "HG00731_same_genloc.txt",
             "HG00732_same_genloc.txt",
             "HG00733_same_genloc.txt",
             "NA19238_same_genloc.txt",
             "NA19239_same_genloc.txt",
             "NA19240_same_genloc.txt"]

# Dictionary of arrays
genlocs = {}

# Read data in files
for file in filenames:
    #print(file)
    with open(file, "r") as f:
        for x in f:
            x = x.rstrip("\n")
            [contig, val1, val2] = x.split(" ")
            if contig not in genlocs:
                genlocs[contig] = []
            genlocs[contig].append(val1)
            genlocs[contig].append(val2)

# Process data
with open("merged_calls.txt", "w") as f:
    f.write("contig" + "\t" + "best chr" + "\t" + "best pos " + "\t" +
            "on target" + "\t" + "total reads" + "\t" + "hits" + "\t" + "percentage" + "\n")
    for contig in genlocs:
        #print(contig)

        # 1.Get the best hit with the biggest number of reads
        [best_chr, best_pos, best_reads, hits] = ["","",0,0]
        for hit in genlocs[contig]:
            hits+=1
            [chr, pos, reads] = hit.split(":")
            if int(reads) > best_reads:
                [best_chr, best_pos, best_reads] = [chr, int(pos), int(reads)]
        print("best hit for contig: ", contig, "from ", hits,
              "hits is on ", best_chr, "position ", best_pos,
              "reads supporting it ", best_reads )

        # 2.Check if reads next to best location account for >50% of the reads
        [on_target, total] = [0,0]
        for hit in genlocs[contig]:
            [chr, pos, reads] = hit.split(":")
            total+=int(reads)
            if best_chr == chr and abs(best_pos - int(pos)) <= max_distance:
                on_target+=int(reads)
                       
        if on_target >= min_on_target and on_target/total >= min_on_target_read_fraction:
            f.write(contig + "\t" + best_chr + "\t" + str(best_pos*1000) + "\t" +
                    str(on_target) + "\t" + str(total) + "\t" + str(hits) + "\t" + str(on_target/total) + "\n")













        
